package com.jsh.erp;

import com.jsh.erp.datasource.entities.User;
import com.jsh.erp.datasource.mappers.UserMapper;
import com.jsh.erp.service.tenant.TenantService;
import com.jsh.erp.service.user.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static com.jsh.erp.constants.BusinessConstants.USER_STATUS_BANNED;
import static com.jsh.erp.constants.BusinessConstants.USER_STATUS_NORMAL;
import static com.jsh.erp.utils.ExceptionCodeConstants.UserExceptionCode.*;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserLoginTest {
    @Autowired
    private UserService userService;

    @MockBean
    private UserMapper mapper;

    @MockBean
    private TenantService tenantService;

    @Test
    public void test() throws Exception {
        given(mapper.selectByExample(any())).willReturn(new ArrayList<>());
        int actual = userService.validateUser("abc", null);
        assertEquals(USER_NOT_EXIST, actual);

        User user = new User();
        user.setStatus(USER_STATUS_BANNED);
        given(mapper.selectByExample(any())).willReturn(Collections.singletonList(user));
        actual = userService.validateUser("abc", "123");
        assertEquals(BLACK_USER, actual);
    }
}
